<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 02 Nov 2017 20:14:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Quality
 * 
 * @property int $id
 * @property string $quality
 * 
 * @property \Illuminate\Database\Eloquent\Collection $shows
 *
 * @package App\Models
 */
class Quality extends Eloquent
{
	protected $table = 'quality';
	public $timestamps = false;

	protected $fillable = [
		'quality'
	];

	public function shows()
	{
		return $this->hasMany(\App\Models\Show::class);
	}
}
