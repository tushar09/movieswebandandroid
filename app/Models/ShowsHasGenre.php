<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 02 Nov 2017 20:14:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ShowsHasGenre
 * 
 * @property int $Shows_id
 * @property int $genre_id
 * 
 * @property \App\Models\Show $show
 * @property \App\Models\Genre $genre
 *
 * @package App\Models
 */
class ShowsHasGenre extends Eloquent
{
	protected $table = 'shows_has_genre';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'Shows_id' => 'int',
		'genre_id' => 'int'
	];

	public function show()
	{
		return $this->belongsTo(\App\Models\Show::class, 'Shows_id');
	}

	public function genre()
	{
		return $this->belongsTo(\App\Models\Genre::class);
	}
}
