<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 02 Nov 2017 20:14:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Year
 * 
 * @property int $id
 * @property string $year
 * 
 * @property \Illuminate\Database\Eloquent\Collection $shows
 *
 * @package App\Models
 */
class Year extends Eloquent
{
	protected $table = 'year';
	public $timestamps = false;

	protected $fillable = [
		'year'
	];

	public function shows()
	{
		return $this->hasMany(\App\Models\Show::class);
	}
}
