<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 02 Nov 2017 20:14:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Star
 * 
 * @property int $id
 * @property string $name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $shows
 *
 * @package App\Models
 */
class Star extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

	public function shows()
	{
		return $this->belongsToMany(\App\Models\Show::class, 'shows_has_stars', 'stars_id', 'Shows_id');
	}
}
