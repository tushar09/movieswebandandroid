<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 02 Nov 2017 20:14:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ShowsHasStar
 * 
 * @property int $Shows_id
 * @property int $stars_id
 * 
 * @property \App\Models\Show $show
 * @property \App\Models\Star $star
 *
 * @package App\Models
 */
class ShowsHasStar extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'Shows_id' => 'int',
		'stars_id' => 'int'
	];

	public function show()
	{
		return $this->belongsTo(\App\Models\Show::class, 'Shows_id');
	}

	public function star()
	{
		return $this->belongsTo(\App\Models\Star::class, 'stars_id');
	}
}
