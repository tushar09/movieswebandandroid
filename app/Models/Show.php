<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 02 Nov 2017 20:14:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Show
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $rating
 * @property int $quality_id
 * @property int $view
 * @property int $downloaded
 * @property string $size
 * @property string $descripction
 * @property string $duration
 * @property int $year_id
 * @property string $url
 * 
 * @property \App\Models\Quality $quality
 * @property \App\Models\Year $year
 * @property \Illuminate\Database\Eloquent\Collection $genres
 * @property \Illuminate\Database\Eloquent\Collection $stars
 *
 * @package App\Models
 */
class Show extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'quality_id' => 'int',
		'view' => 'int',
		'downloaded' => 'int',
		'year_id' => 'int'
	];

	protected $fillable = [
		'name',
		'email',
		'rating',
		'quality_id',
		'view',
		'downloaded',
		'size',
		'descripction',
		'duration',
		'year_id',
		'url'
	];

	public function quality()
	{
		return $this->belongsTo(\App\Models\Quality::class);
	}

	public function year()
	{
		return $this->belongsTo(\App\Models\Year::class);
	}

	public function genres()
	{
		return $this->belongsToMany(\App\Models\Genre::class, 'shows_has_genre', 'Shows_id');
	}

	public function stars()
	{
		return $this->belongsToMany(\App\Models\Star::class, 'shows_has_stars', 'Shows_id', 'stars_id');
	}
}
