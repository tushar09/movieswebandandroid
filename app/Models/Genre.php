<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 02 Nov 2017 20:14:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Genre
 * 
 * @property int $id
 * @property string $genre
 * 
 * @property \Illuminate\Database\Eloquent\Collection $shows
 *
 * @package App\Models
 */
class Genre extends Eloquent
{
	protected $table = 'genre';
	public $timestamps = false;

	protected $fillable = [
		'genre'
	];

	public function shows()
	{
		return $this->belongsToMany(\App\Models\Show::class, 'shows_has_genre', 'genre_id', 'Shows_id');
	}
}
